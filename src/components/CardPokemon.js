import React, { Component } from 'react'

export class CardPokemon extends Component {

    constructor() {
        super()
        this.state = { 
            error: null,
            isLoaded: false,
        };
    }

    componentDidMount() {
        fetch(this.props.item.url)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                pokemon: result.sprites.front_shiny,
                
              });
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }

  render() {
    const { error, isLoaded, pokemon } = this.state;
    return (
        <div className='card'>
            <img src={pokemon}></img>
            <span>{this.props.item.name}</span>
        </div>
    )
  }
}

export default CardPokemon