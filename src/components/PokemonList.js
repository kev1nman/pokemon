import React, { Component } from 'react';
import CardPokemon from './CardPokemon';

import "./../styles/pokemonList.css"


class PokemonList extends Component {

    constructor() {
        super()
        this.state = { 
            error: null,
      isLoaded: false,
            pokemonList: []
        };
    }


    componentDidMount() {
        fetch("https://pokeapi.co/api/v2/pokemon?limit=100")
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                pokemonList: result.results,
                
              });
            },
            // Nota: es importante manejar errores aquí y no en 
            // un bloque catch() para que no interceptemos errores
            // de errores reales en los componentes.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }

      render() {
        const { error, isLoaded, pokemonList } = this.state;
        if (error) {
          return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
          return <div>Loading...</div>;
        } else {
          return (
            <div className='box-cards'>
              {pokemonList.map((item, i) => (
                <div key={i}>
                  <CardPokemon item={item}/>
                </div>
              ))}
            </div>
          );
        }
      }
}

export default PokemonList;