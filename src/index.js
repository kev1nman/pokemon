import React from 'react';
import ReactDOM from 'react-dom';
import PokemonList from "./components/PokemonList"
import "./styles/index.css"

ReactDOM.render(
  <React.StrictMode>
    <PokemonList/>
  </React.StrictMode>,
  document.getElementById('root')
);


